#!/bin/bash 

set -e

cd /var/www

echo "Installing dependencies..."
composer install

echo "Running laravel migrations..."
php artisan migrate --force

echo "Start server"
/usr/sbin/apachectl -D FOREGROUND