FROM php:5.6-apache

LABEL Joel Chinda <chindajoel@gmail.com>

RUN echo "deb http://archive.debian.org/debian stretch main" > /etc/apt/sources.list
RUN apt-get update
RUN apt-get install -y libmcrypt-dev g++ libicu-dev libmcrypt4 libcurl4-gnutls-dev
RUN apt-get install -y libzip-dev zlib1g-dev
RUN docker-php-ext-install pdo_mysql mcrypt mbstring intl zip curl mbstring json

RUN apt install -y nano git wget

RUN wget https://getcomposer.org/download/latest-2.2.x/composer.phar
RUN chmod +x ./composer.phar
RUN cp ./composer.phar /usr/bin/composer

RUN apt-get install -y libxml2-dev libpq-dev zip unzip  libpng-dev

RUN docker-php-ext-install xml pgsql  mysql pdo pdo_pgsql tokenizer gd

RUN apt-get purge --auto-remove -y libmcrypt-dev g++ libicu-dev

RUN a2enmod rewrite
RUN rm -rf /var/lib/apt/lists/*


RUN mkdir -p /etc/service/apache2  /var/log/apache2 ; sync 
#COPY apache2.sh /etc/service/apache2/run
RUN chown -R www-data /var/log/apache2


#COPY apache2.conf /etc/apache2/apache2.conf

WORKDIR /var
COPY apache/000-default.conf /etc/apache2/sites-enabled/000-default.conf
COPY php/php.ini /usr/local/etc/php/php.ini
COPY scripts/startup.sh /var
RUN chmod +x startup.sh

#RUN sed  -i 's/DocumentRoot \/var\/www\/html/DocumentRoot \/var\/www/' /etc/apache2/sites-available/000-default.conf

#COPY pre-conf.sh /sbin/pre-conf
# RUN chmod +x /sbin/pre-conf; sync \
#     && /bin/bash -c /sbin/pre-conf \
#     && rm /sbin/pre-conf

RUN usermod -u 1000 www-data

ENV APACHE_RUN_USER www-data
ENV APACHE_RUN_GROUP www-data
ENV APACHE_LOG_DIR /var/log/apache2
ENV APACHE_LOCK_DIR /var/lock/apache2
ENV APACHE_PID_FILE /var/run/apache2.pid

WORKDIR /var/www

VOLUME ["/var/www"]

EXPOSE 80

CMD /var/startup.sh
#CMD ["service apache2 start" "-D", "FOREGROUND"]